# British English translation for gnome-initial-setup.
# Copyright (C) 2012 gnome-initial-setup's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-initial-setup package.
# Bruce Cowan <bruce@bcowan.eu>, 2012, 2013, 2017.
# Zander Brown <zbrown@gnome.org>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-initial-setup master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-initial-setup/"
"issues\n"
"POT-Creation-Date: 2019-06-06 11:38+0000\n"
"PO-Revision-Date: 2019-08-25 01:40+0100\n"
"Last-Translator: Zander Brown <zbrown@gnome.org>\n"
"Language-Team: English - United Kingdom <en_GB@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Gtranslator 3.32.1\n"
"X-Project-Style: gnome\n"

#: data/gnome-initial-setup-first-login.desktop.in.in:3
#: data/gnome-initial-setup.desktop.in.in:3
msgid "Initial Setup"
msgstr "Initial Setup"

#: gnome-initial-setup/gis-assistant.c:423
msgid "_Next"
msgstr "_Next"

#: gnome-initial-setup/gis-assistant.c:424
msgid "_Accept"
msgstr "_Accept"

#: gnome-initial-setup/gis-assistant.c:425
msgid "_Skip"
msgstr "_Skip"

#: gnome-initial-setup/gis-assistant.c:426
msgid "_Previous"
msgstr "_Previous"

#: gnome-initial-setup/gis-assistant.c:427
msgid "_Cancel"
msgstr "_Cancel"

#: gnome-initial-setup/gnome-initial-setup.c:259
msgid "Force existing user mode"
msgstr "Force existing user mode"

#: gnome-initial-setup/gnome-initial-setup.c:265
msgid "— GNOME initial setup"
msgstr "— GNOME initial setup"

#: gnome-initial-setup/pages/account/gis-account-avatar-chooser.ui:36
#| msgid "Take a photo…"
msgid "Take a Picture…"
msgstr "Take a photo…"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:182
msgid "Failed to register account"
msgstr "Failed to register account"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:376
msgid "No supported way to authenticate with this domain"
msgstr "No supported way to authenticate with this domain"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:416
msgid "Failed to join domain"
msgstr "Failed to join domain"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:484
msgid "Failed to log into domain"
msgstr "Failed to log into domain"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:31
msgid "Enterprise Login"
msgstr "Enterprise Login"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:45
msgid ""
"Enterprise login allows an existing centrally managed user account to be "
"used on this device. You can also use this account to access company "
"resources on the internet."
msgstr ""
"Enterprise login allows an existing centrally managed user account to be "
"used on this device. You can also use this account to access company "
"resources on the internet."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:65
#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:316
msgid "_Domain"
msgstr "_Domain"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:82
#: gnome-initial-setup/pages/account/gis-account-page-local.ui:101
msgid "_Username"
msgstr "_Username"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:99
#: gnome-initial-setup/pages/password/gis-password-page.ui:66
msgid "_Password"
msgstr "_Password"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:175
msgid "Enterprise domain or realm name"
msgstr "Enterprise domain or realm name"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:240
msgid "C_ontinue"
msgstr "C_ontinue"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:275
msgid "Domain Administrator Login"
msgstr "Domain Administrator Login"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:295
msgid ""
"In order to use enterprise logins, this computer needs to be enrolled in a "
"domain. Please have your network administrator type the domain password "
"here, and choose a unique computer name for your computer."
msgstr ""
"In order to use enterprise logins, this computer needs to be enrolled in a "
"domain. Please have your network administrator type the domain password "
"here, and choose a unique computer name for your computer."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:350
msgid "_Computer"
msgstr "_Computer"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:382
msgid "Administrator _Name"
msgstr "Administrator _Name"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:416
msgid "Administrator Password"
msgstr "Administrator Password"

#: gnome-initial-setup/pages/account/gis-account-page-local.c:203
msgid "Please check the name and username. You can choose a picture too."
msgstr "Please check the name and username. You can choose a picture too."

#: gnome-initial-setup/pages/account/gis-account-page-local.c:422
msgid "We need a few details to complete setup."
msgstr "We need a few details to complete setup."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:21
msgid "Avatar image"
msgstr "Avatar image"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:41
#: gnome-initial-setup/pages/account/gis-account-page.c:264
msgid "About You"
msgstr "About You"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:53
msgid "Please provide a name and username. You can choose a picture too."
msgstr "Please provide a name and username. You can choose a picture too."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:70
msgid "_Full Name"
msgstr "_Full Name"

#: gnome-initial-setup/pages/account/gis-account-page.ui:43
#| msgid "Enterprise Login"
msgid "_Enterprise Login"
msgstr "_Enterprise Login"

#: gnome-initial-setup/pages/account/gis-account-page.ui:53
#| msgid "Set Up _Enterprise Login"
msgid "Go online to set up Enterprise Login."
msgstr "Go online to set up Enterprise Login."

#: gnome-initial-setup/pages/account/um-realm-manager.c:310
msgid "Cannot automatically join this type of domain"
msgstr "Cannot automatically join this type of domain"

#: gnome-initial-setup/pages/account/um-realm-manager.c:373
#, c-format
msgid "No such domain or realm found"
msgstr "No such domain or realm found"

#: gnome-initial-setup/pages/account/um-realm-manager.c:782
#: gnome-initial-setup/pages/account/um-realm-manager.c:796
#, c-format
msgid "Cannot log in as %s at the %s domain"
msgstr "Cannot log in as %s at the %s domain"

#: gnome-initial-setup/pages/account/um-realm-manager.c:788
msgid "Invalid password, please try again"
msgstr "Invalid password, please try again"

#: gnome-initial-setup/pages/account/um-realm-manager.c:801
#, c-format
msgid "Couldn’t connect to the %s domain: %s"
msgstr "Couldn’t connect to the %s domain: %s"

#: gnome-initial-setup/pages/account/um-utils.c:245
msgid "Sorry, that user name isn’t available. Please try another."
msgstr "Sorry, that user name isn’t available. Please try another."

#: gnome-initial-setup/pages/account/um-utils.c:248
#, c-format
msgid "The username is too long."
msgstr "The username is too long."

#: gnome-initial-setup/pages/account/um-utils.c:251
msgid "The username cannot start with a “-”."
msgstr "The username cannot start with a “-”."

#: gnome-initial-setup/pages/account/um-utils.c:254
msgid ""
"The username should only consist of upper and lower case letters from a-z, "
"digits and the following characters: . - _"
msgstr ""
"The username should only consist of upper and lower case letters from a-z, "
"digits and the following characters: . - _"

#: gnome-initial-setup/pages/account/um-utils.c:258
msgid "This will be used to name your home folder and can’t be changed."
msgstr "This will be used to name your home folder and can’t be changed."

#: gnome-initial-setup/pages/eulas/gis-eula-page.c:301
#: gnome-initial-setup/pages/eulas/gis-eula-page.ui:21
msgid "License Agreements"
msgstr "Licence Agreements"

#: gnome-initial-setup/pages/eulas/gis-eula-page.ui:50
msgid ""
"I have _agreed to the terms and conditions in this end user license "
"agreement."
msgstr ""
"I have _agreed to the terms and conditions in this end user licence "
"agreement."

#: gnome-initial-setup/pages/goa/gis-goa-page.c:87
msgid "Add Account"
msgstr "Add Account"

#: gnome-initial-setup/pages/goa/gis-goa-page.c:339
msgid "Online Accounts"
msgstr "Online Accounts"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:39
msgid "Connect Your Online Accounts"
msgstr "Connect Your Online Accounts"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:58
msgid ""
"Connect your accounts to easily access your email, online calendar, "
"contacts, documents and photos."
msgstr ""
"Connect your accounts to easily access your e-mail, online calendar, "
"contacts, documents and photos."

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:95
msgid ""
"Accounts can be added and removed at any time from the Settings application."
msgstr ""
"Accounts can be added and removed at any time from the Settings application."

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:240
msgid "Preview"
msgstr "Preview"

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:299
#: gnome-initial-setup/pages/language/cc-language-chooser.c:222
#: gnome-initial-setup/pages/region/cc-region-chooser.c:210
msgid "More…"
msgstr "More…"

#. Translators: a search for input methods or keyboard layouts
#. * did not yield any results
#.
#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:321
msgid "No inputs found"
msgstr "No inputs found"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.c:480
#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:30
msgid "Typing"
msgstr "Typing"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:42
msgid "Select your keyboard layout or an input method."
msgstr "Select your keyboard layout or an input method."

#: gnome-initial-setup/pages/language/cc-language-chooser.c:238
msgid "No languages found"
msgstr "No languages found"

#: gnome-initial-setup/pages/language/gis-language-page.c:304
msgid "Welcome"
msgstr "Welcome"

#. Translators: This is meant to be a warm, engaging welcome message,
#. * like greeting somebody at the door. If the exclamation mark is not
#. * suitable for this in your language you may replace it.
#.
#: gnome-initial-setup/pages/language/gis-welcome-widget.c:132
msgid "Welcome!"
msgstr "Welcome!"

#: gnome-initial-setup/pages/network/gis-network-page.c:313
msgctxt "Wireless access point"
msgid "Other…"
msgstr "Other…"

#: gnome-initial-setup/pages/network/gis-network-page.c:398
msgid "Wireless networking is disabled"
msgstr "Wireless networking is disabled"

#: gnome-initial-setup/pages/network/gis-network-page.c:405
msgid "Checking for available wireless networks"
msgstr "Checking for available wireless networks"

#: gnome-initial-setup/pages/network/gis-network-page.c:713
msgid "Network"
msgstr "Network"

#: gnome-initial-setup/pages/network/gis-network-page.ui:33
msgid "Wi-Fi"
msgstr "Wi-Fi"

#: gnome-initial-setup/pages/network/gis-network-page.ui:45
msgid ""
"Connecting to the Internet will enable you to set the time, add your "
"details, and enable you to access your email, calendar, and contacts. It is "
"also necessary for enterprise login accounts."
msgstr ""
"Connecting to the Internet will enable you to set the time, add your "
"details, and enable you to access your e-mail, calendar, and contacts. It is "
"also necessary for enterprise login accounts."

#: gnome-initial-setup/pages/network/gis-network-page.ui:103
msgid "No wireless available"
msgstr "No wireless available"

#: gnome-initial-setup/pages/network/gis-network-page.ui:118
msgid "Turn On"
msgstr "Turn On"

#: gnome-initial-setup/pages/password/gis-password-page.c:142
msgid "This is a weak password."
msgstr "This is a weak password."

#: gnome-initial-setup/pages/password/gis-password-page.c:148
msgid "The passwords do not match."
msgstr "The passwords do not match."

#: gnome-initial-setup/pages/password/gis-password-page.c:270
msgid "Password"
msgstr "Password"

#: gnome-initial-setup/pages/password/gis-password-page.ui:34
msgid "Set a Password"
msgstr "Set a Password"

#: gnome-initial-setup/pages/password/gis-password-page.ui:47
msgid "Be careful not to lose your password."
msgstr "Be careful not to lose your password."

#: gnome-initial-setup/pages/password/gis-password-page.ui:98
msgid "_Confirm"
msgstr "_Confirm"

#: gnome-initial-setup/pages/password/pw-utils.c:81
msgctxt "Password hint"
msgid "The new password needs to be different from the old one."
msgstr "The new password needs to be different from the old one."

#: gnome-initial-setup/pages/password/pw-utils.c:83
#| msgctxt "Password hint"
#| msgid "Try changing some letters and numbers."
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing some letters "
"and numbers."
msgstr ""
"This password is very similar to your last one. Try changing some letters "
"and numbers."

#: gnome-initial-setup/pages/password/pw-utils.c:85
#: gnome-initial-setup/pages/password/pw-utils.c:93
#| msgctxt "Password hint"
#| msgid "Try changing the password a bit more."
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing the password a "
"bit more."
msgstr ""
"This password is very similar to your last one. Try changing the password a "
"bit more."

#: gnome-initial-setup/pages/password/pw-utils.c:87
#| msgctxt "Password hint"
#| msgid "A password without your user name would be stronger."
msgctxt "Password hint"
msgid ""
"This is a weak password. A password without your user name would be stronger."
msgstr ""
"This is a weak password. A password without your user name would be stronger."

#: gnome-initial-setup/pages/password/pw-utils.c:89
#| msgctxt "Password hint"
#| msgid "Try to avoid using your name in the password."
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid using your name in the password."
msgstr "This is a weak password. Try to avoid using your name in the password."

#: gnome-initial-setup/pages/password/pw-utils.c:91
#| msgctxt "Password hint"
#| msgid "Try to avoid some of the words included in the password."
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid some of the words included in the "
"password."
msgstr ""
"This is a weak password. Try to avoid some of the words included in the "
"password."

#: gnome-initial-setup/pages/password/pw-utils.c:95
#| msgctxt "Password hint"
#| msgid ""
#| "This is a weak password. Try to add more letters, numbers and symbols."
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid common words."
msgstr "This is a weak password. Try to avoid common words."

#: gnome-initial-setup/pages/password/pw-utils.c:97
#| msgctxt "Password hint"
#| msgid "Try to avoid reordering existing words."
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid reordering existing words."
msgstr "This is a weak password. Try to avoid reordering existing words."

#: gnome-initial-setup/pages/password/pw-utils.c:99
#| msgctxt "Password hint"
#| msgid ""
#| "This is a weak password. Try to add more letters, numbers and symbols."
msgctxt "Password hint"
msgid "This is a weak password. Try to use more numbers."
msgstr "This is a weak password. Try to use more numbers."

#: gnome-initial-setup/pages/password/pw-utils.c:101
#| msgctxt "Password hint"
#| msgid "Try to use more uppercase letters."
msgctxt "Password hint"
msgid "This is a weak password. Try to use more uppercase letters."
msgstr "This is a weak password. Try to use more uppercase letters."

#: gnome-initial-setup/pages/password/pw-utils.c:103
#| msgctxt "Password hint"
#| msgid "Try to use more lowercase letters."
msgctxt "Password hint"
msgid "This is a weak password. Try to use more lowercase letters."
msgstr "This is a weak password. Try to use more lowercase letters."

#: gnome-initial-setup/pages/password/pw-utils.c:105
#| msgctxt "Password hint"
#| msgid "Try to use more special characters, like punctuation."
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use more special characters, like "
"punctuation."
msgstr ""
"This is a weak password. Try to use more special characters, like "
"punctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:107
#| msgctxt "Password hint"
#| msgid "Try to use a mixture of letters, numbers and punctuation."
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use a mixture of letters, numbers and "
"punctuation."
msgstr ""
"This is a weak password. Try to use a mixture of letters, numbers and "
"punctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:109
#| msgctxt "Password hint"
#| msgid "Try to avoid repeating the same character."
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid repeating the same character."
msgstr "This is a weak password. Try to avoid repeating the same character."

#: gnome-initial-setup/pages/password/pw-utils.c:111
#| msgctxt "Password hint"
#| msgid ""
#| "Try to avoid repeating the same type of character: you need to mix up "
#| "letters, numbers and punctuation."
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid repeating the same type of character: "
"you need to mix up letters, numbers and punctuation."
msgstr ""
"This is a weak password. Try to avoid repeating the same type of character: "
"you need to mix up letters, numbers and punctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:113
#| msgctxt "Password hint"
#| msgid "Try to avoid sequences like 1234 or abcd."
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid sequences like 1234 or abcd."
msgstr "This is a weak password. Try to avoid sequences like 1234 or abcd."

#: gnome-initial-setup/pages/password/pw-utils.c:115
#| msgctxt "Password hint"
#| msgid ""
#| "This is a weak password. Try to add more letters, numbers and symbols."
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to add more letters, numbers and punctuation."
msgstr ""
"This is a weak password. Try to add more letters, numbers and punctuation."

#: gnome-initial-setup/pages/password/pw-utils.c:117
msgctxt "Password hint"
msgid "Mix uppercase and lowercase and try to use a number or two."
msgstr "Mix uppercase and lowercase and try to use a number or two."

#: gnome-initial-setup/pages/password/pw-utils.c:119
msgctxt "Password hint"
msgid ""
"Adding more letters, numbers and punctuation will make the password stronger."
msgstr ""
"Adding more letters, numbers and punctuation will make the password stronger."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:110
#, c-format
msgid ""
"Sending reports of technical problems helps us to improve %s. Reports are "
"sent anonymously and are scrubbed of personal data."
msgstr ""
"Sending reports of technical problems helps us to improve %s. Reports are "
"sent anonymously and are scrubbed of personal data."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:120
#, c-format
msgid "Problem data will be collected by %s:"
msgstr "Problem data will be collected by %s:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:121
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:181
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:256
msgid "Privacy Policy"
msgstr "Privacy Policy"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:181
msgid "Uses Mozilla Location Service:"
msgstr "Uses Mozilla Location Service:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:292
#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:32
msgid "Privacy"
msgstr "Privacy"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:49
msgid "Location Services"
msgstr "Location Services"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:70
msgid ""
"Allows applications to determine your geographical location. An indication "
"is shown when location services are in use."
msgstr ""
"Allows applications to determine your geographical location. An indication "
"is shown when location services are in use."

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:92
msgid "Automatic Problem Reporting"
msgstr "Automatic Problem Reporting"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:128
msgid ""
"Privacy controls can be changed at any time from the Settings application."
msgstr ""
"Privacy controls can be changed at any time from the Settings application."

#: gnome-initial-setup/pages/region/cc-region-chooser.c:227
msgid "No regions found"
msgstr "No regions found"

#: gnome-initial-setup/pages/region/gis-region-page.c:223
#: gnome-initial-setup/pages/region/gis-region-page.ui:31
msgid "Region"
msgstr "Region"

#: gnome-initial-setup/pages/region/gis-region-page.ui:44
msgid "Choose your country or region."
msgstr "Choose your country or region."

#: gnome-initial-setup/pages/software/gis-software-page.c:184
#| msgid "Software Sources"
msgid "Software Repositories"
msgstr "Software Sources"

#. TRANSLATORS: this is the third party repositories info bar.
#: gnome-initial-setup/pages/software/gis-software-page.c:188
msgid "Access additional software from selected third party sources."
msgstr "Access additional software from selected third party sources."

#. TRANSLATORS: this is the third party repositories info bar.
#: gnome-initial-setup/pages/software/gis-software-page.c:192
msgid ""
"Some of this software is proprietary and therefore has restrictions on use, "
"sharing, and access to source code."
msgstr ""
"Some of this software is proprietary and therefore has restrictions on use, "
"sharing, and access to source code."

#: gnome-initial-setup/pages/software/gis-software-page.ui:31
#| msgid "Additional Software Sources"
msgid "Additional Software Repositories"
msgstr "Additional Software Sources"

#: gnome-initial-setup/pages/software/gis-software-page.ui:55
msgid "<a href=\"more\">Find out more…</a>"
msgstr "<a href=\"more\">Find out more…</a>"

#: gnome-initial-setup/pages/software/gis-software-page.ui:69
msgid "Third Party Repositories"
msgstr "Third Party Software Sources"

#: gnome-initial-setup/pages/software/gis-software-page.ui:165
msgid ""
"Proprietary software typically has restrictions on how it can be used and on "
"access to source code. This prevents anyone but the software owner from "
"inspecting, improving or learning from its code."
msgstr ""
"Proprietary software typically has restrictions on how it can be used and on "
"access to source code. This prevents anyone but the software owner from "
"inspecting, improving or learning from its code."

#: gnome-initial-setup/pages/software/gis-software-page.ui:177
msgid ""
"In contrast, Free Software can be freely run, copied, distributed, studied "
"and modified."
msgstr ""
"In contrast, Free Software can be freely run, copied, distributed, studied "
"and modified."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME 3" if we can't
#. * detect any distribution.
#: gnome-initial-setup/pages/summary/gis-summary-page.c:271
#, c-format
msgid "_Start Using %s"
msgstr "_Start Using %s"

#: gnome-initial-setup/pages/summary/gis-summary-page.c:301
msgid "Ready to Go"
msgstr "Ready to Go"

#: gnome-initial-setup/pages/summary/gis-summary-page.ui:64
msgid "You’re ready to go!"
msgstr "You’re ready to go!"

#. Translators: "city, country"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:236
#, c-format
msgctxt "timezone loc"
msgid "%s, %s"
msgstr "%s, %s"

#. Translators: UTC here means the Coordinated Universal Time.
#. * %:::z will be replaced by the offset from UTC e.g. UTC+02
#.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:273
msgid "UTC%:::z"
msgstr "UTC%:::z"

#. Translators: This is the time format used in 12-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:277
msgid "%l:%M %p"
msgstr "%l:%M %p"

#. Translators: This is the time format used in 24-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:280
msgid "%R"
msgstr "%R"

#. Translators: "timezone (utc shift)"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:283
#, c-format
msgctxt "timezone map"
msgid "%s (%s)"
msgstr "%s (%s)"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:426
#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:33
msgid "Time Zone"
msgstr "Time Zone"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:53
msgid ""
"The time zone will be set automatically if your location can be found. You "
"can also search for a city to set it yourself."
msgstr ""
"The time zone will be set automatically if your location can be found. You "
"can also search for a city to set it yourself."

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:82
msgid "Please search for a nearby city"
msgstr "Please search for a nearby city"

#~ msgid "Disable image"
#~ msgstr "Disable image"

#~| msgctxt "Password hint"
#~| msgid "This is a weak password. Try to avoid common words."
#~ msgctxt "Password hint"
#~ msgid "Try to avoid common words."
#~ msgstr "Try to avoid common words."

#~| msgctxt "Password hint"
#~| msgid "This is a weak password. Try to use more numbers."
#~ msgctxt "Password hint"
#~ msgid "Try to use more numbers."
#~ msgstr "Try to use more numbers."

#~| msgctxt "Password hint"
#~| msgid ""
#~| "This is a weak password. Try to use a mixture of letters, numbers and "
#~| "punctuation."
#~ msgctxt "Password hint"
#~ msgid ""
#~ "Password needs to be longer. Try to add more letters, numbers and "
#~ "punctuation."
#~ msgstr ""
#~ "Password needs to be longer. Try to add more letters, numbers and "
#~ "punctuation."

#~ msgid ""
#~ "Proprietary software sources provide access to additional software, "
#~ "including web browsers and games. This software typically has "
#~ "restrictions on use and access to source code, and is not provided by %s."
#~ msgstr ""
#~ "Proprietary software sources provide access to additional software, "
#~ "including web browsers and games. This software typically has "
#~ "restrictions on use and access to source code, and is not provided by %s."

#~ msgid "Proprietary Software Sources"
#~ msgstr "Proprietary Software Sources"

#~ msgid "A user with the username '%s' already exists."
#~ msgstr "A user with the username '%s' already exists."

#~ msgid "_Verify"
#~ msgstr "_Verify"

#~ msgctxt "Password strength"
#~ msgid "Strength: Weak"
#~ msgstr "Strength: Weak"

#~ msgctxt "Password strength"
#~ msgid "Strength: Low"
#~ msgstr "Strength: Low"

#~ msgctxt "Password strength"
#~ msgid "Strength: Medium"
#~ msgstr "Strength: Medium"

#~ msgctxt "Password strength"
#~ msgid "Strength: Good"
#~ msgstr "Strength: Good"

#~ msgctxt "Password strength"
#~ msgid "Strength: High"
#~ msgstr "Strength: High"

#~ msgid "_Back"
#~ msgstr "_Back"

#~ msgid "Login"
#~ msgstr "Login"

#~ msgid "Create a Local Account"
#~ msgstr "Create a Local Account"

#~ msgid "page 1"
#~ msgstr "page 1"

#~ msgctxt "Password strength"
#~ msgid "Too short"
#~ msgstr "Too short"

#~ msgctxt "Password strength"
#~ msgid "Not good enough"
#~ msgstr "Not good enough"

#~ msgctxt "Password strength"
#~ msgid "Weak"
#~ msgstr "Weak"

#~ msgctxt "Password strength"
#~ msgid "Fair"
#~ msgstr "Fair"

#~ msgctxt "Password strength"
#~ msgid "Good"
#~ msgstr "Good"

#~ msgctxt "Password strength"
#~ msgid "Strong"
#~ msgstr "Strong"

#~ msgid "Error creating account"
#~ msgstr "Error creating account"

#~ msgid "Error removing account"
#~ msgstr "Error removing account"

#~ msgid "Are you sure you want to remove the account?"
#~ msgstr "Are you sure you want to remove the account?"

#~ msgid "This will not remove the account on the server."
#~ msgstr "This will not remove the account on the server."

#~ msgid "_Remove"
#~ msgstr "_Remove"

#~ msgid "Connect to your existing data in the cloud"
#~ msgstr "Connect to your existing data in the cloud"

#~ msgid ""
#~ "Adding accounts will allow you to transparently connect to your online "
#~ "photos, contacts, mail, and more."
#~ msgstr ""
#~ "Adding accounts will allow you to transparently connect to your online "
#~ "photos, contacts, mail and more."

#~ msgid "Keyboard Layout"
#~ msgstr "Keyboard Layout"

#~ msgid "Select input sources"
#~ msgstr "Select input sources"

#~ msgid "Add Input Source"
#~ msgstr "Add Input Source"

#~ msgid "Remove Input Source"
#~ msgstr "Remove Input Source"

#~ msgid "Move Input Source Up"
#~ msgstr "Move Input Source Up"

#~ msgid "Move Input Source Down"
#~ msgstr "Move Input Source Down"

#~ msgid "Input Source Settings"
#~ msgstr "Input Source Settings"

#~ msgid "Show Keyboard Layout"
#~ msgstr "Show Keyboard Layout"

#~ msgid "Select an input source"
#~ msgstr "Select an input source"

#~ msgid "Use %s"
#~ msgstr "Use %s"

#~ msgid "Search for a location"
#~ msgstr "Search for a location"

#~ msgid "_Determine your location automatically"
#~ msgstr "_Determine your location automatically"

#~ msgid "No network devices found."
#~ msgstr "No network devices found."

#~ msgid "Thank You"
#~ msgstr "Thank You"

#~ msgid "You may change these options at any time in Settings."
#~ msgstr "You may change these options at any time in Settings."

#~ msgid "Create Local Account"
#~ msgstr "Create Local Account"

#~ msgid "_Done"
#~ msgstr "_Done"

#~ msgid "_Require a password to use this account"
#~ msgstr "_Require a password to use this account"

#~ msgid "_Act as administrator of this computer"
#~ msgstr "_Act as administrator of this computer"

#~ msgid "Choose How to Login"
#~ msgstr "Choose How to Login"

#~ msgid "Remove"
#~ msgstr "Remove"

#~ msgid "English"
#~ msgstr "English"

#~ msgid "British English"
#~ msgstr "British English"

#~ msgid "German"
#~ msgstr "German"

#~ msgid "French"
#~ msgstr "French"

#~ msgid "Spanish"
#~ msgstr "Spanish"

#~ msgid "Chinese (simplified)"
#~ msgstr "Chinese (simplified)"

#~ msgid "Unspecified"
#~ msgstr "Unspecified"

#~ msgid "Show _all"
#~ msgstr "Show _all"

#~ msgid "Enjoy GNOME!"
#~ msgstr "Enjoy GNOME!"

#~ msgid "New to GNOME 3 and need help finding your way around?"
#~ msgstr "New to GNOME 3 and need help finding your way around?"

#~ msgid "_Take a Tour"
#~ msgstr "_Take a Tour"
