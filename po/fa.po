# Persian translation for gnome-initial-setup.
# Copyright (C) 2012 gnome-initial-setup's COPYRIGHT HOLDER
# This file is distributed under the same license as the gnome-initial-setup package.
# Arash Mousavi <mousavi.arash@gmail.com>, 2012, 2013, 2014, 2015. 2016, 2017.
# Danial Behzadi <dani.behzi@ubuntu.com>, 2014.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-initial-setup master\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/gnome-initial-setup/"
"issues\n"
"POT-Creation-Date: 2019-06-06 11:38+0000\n"
"PO-Revision-Date: 2019-09-02 14:35+0430\n"
"Last-Translator: Danial Behzadi <dani.behzi@ubuntu.com>\n"
"Language-Team: Persian\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2.1\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: data/gnome-initial-setup-first-login.desktop.in.in:3
#: data/gnome-initial-setup.desktop.in.in:3
msgid "Initial Setup"
msgstr "برپاسازی نخستین"

#: gnome-initial-setup/gis-assistant.c:423
msgid "_Next"
msgstr "_بعدی"

#: gnome-initial-setup/gis-assistant.c:424
msgid "_Accept"
msgstr "_قبول"

#: gnome-initial-setup/gis-assistant.c:425
msgid "_Skip"
msgstr "_پرش"

#: gnome-initial-setup/gis-assistant.c:426
msgid "_Previous"
msgstr "_پیشین"

#: gnome-initial-setup/gis-assistant.c:427
msgid "_Cancel"
msgstr "_لغو"

#: gnome-initial-setup/gnome-initial-setup.c:259
msgid "Force existing user mode"
msgstr "اجبار به حالت کاربر موجود"

#: gnome-initial-setup/gnome-initial-setup.c:265
msgid "— GNOME initial setup"
msgstr "— راه‌اندازی نخستین گنوم"

#: gnome-initial-setup/pages/account/gis-account-avatar-chooser.ui:36
msgid "Take a Picture…"
msgstr "عکسی بگیرید…"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:182
msgid "Failed to register account"
msgstr "شکست در ثبت حساب"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:376
msgid "No supported way to authenticate with this domain"
msgstr "هیچ روش پشتیبانی شده‌ای برای تصدیق هویت با این دامنه وجود ندارد"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:416
msgid "Failed to join domain"
msgstr "پیوستن به دامنه شکست خورد"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.c:484
msgid "Failed to log into domain"
msgstr "ورود به دامنه شکست خورد"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:31
msgid "Enterprise Login"
msgstr "ورود تجاری"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:45
msgid ""
"Enterprise login allows an existing centrally managed user account to be used "
"on this device. You can also use this account to access company resources on "
"the internet."
msgstr ""
"ورود تجاری اجازه می‌دهد از یک حساب کاربری مدیریت‌شدهٔ مرکزی موجود برای این دستگاه "
"استفاده شود. شما همچنین از این حساب می‌توانید برای دسترسی به منابع شرکت در "
"اینترنت استفاده کنید."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:65
#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:316
msgid "_Domain"
msgstr "_دامنه"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:82
#: gnome-initial-setup/pages/account/gis-account-page-local.ui:101
msgid "_Username"
msgstr "_نام‌کاربری"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:99
#: gnome-initial-setup/pages/password/gis-password-page.ui:66
msgid "_Password"
msgstr "_گذرواژه"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:175
msgid "Enterprise domain or realm name"
msgstr "نام دامنه یا ناحیه تجاری"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:240
msgid "C_ontinue"
msgstr "ا_دامه"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:275
msgid "Domain Administrator Login"
msgstr "ورود مدیر دامنه"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:295
msgid ""
"In order to use enterprise logins, this computer needs to be enrolled in a "
"domain. Please have your network administrator type the domain password here, "
"and choose a unique computer name for your computer."
msgstr ""
"جهت استفاده از ورود‌های تجاری، لازم است این رایانه در دامنه‌ای ثبت شود. لطفاًً از "
"مدیر شبکهٔ خود بخواهید که گذرواژهٔ دامنه را این‌جا وارد کرده و یک نام رایانهٔ "
"اختصاصی برای این سیستم انتخاب کند."

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:350
msgid "_Computer"
msgstr "_رایانه"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:382
msgid "Administrator _Name"
msgstr "_نام مدیر"

#: gnome-initial-setup/pages/account/gis-account-page-enterprise.ui:416
msgid "Administrator Password"
msgstr "گذرواژهٔ مدیر"

#: gnome-initial-setup/pages/account/gis-account-page-local.c:203
msgid "Please check the name and username. You can choose a picture too."
msgstr "لطفاً نام و نام‌کاربری را بررسی کنید. می‌توانید تصویری هم برگزینید."

#: gnome-initial-setup/pages/account/gis-account-page-local.c:422
msgid "We need a few details to complete setup."
msgstr "برای کامل کردن راه‌اندازی نیاز به کمی جزئیات داریم."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:21
msgid "Avatar image"
msgstr "عکس آواتار"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:41
#: gnome-initial-setup/pages/account/gis-account-page.c:264
msgid "About You"
msgstr "دربارهٔ شما"

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:53
msgid "Please provide a name and username. You can choose a picture too."
msgstr "لطفاً یک نام و نام‌کاربری وارد کنید. می‌توانید تصویری هم برگزینید."

#: gnome-initial-setup/pages/account/gis-account-page-local.ui:70
msgid "_Full Name"
msgstr "_نام کامل"

#: gnome-initial-setup/pages/account/gis-account-page.ui:43
msgid "_Enterprise Login"
msgstr "ورود _تجاری"

#: gnome-initial-setup/pages/account/gis-account-page.ui:53
msgid "Go online to set up Enterprise Login."
msgstr "برای برپاسازی ورود تجاری برخط شوید"

#: gnome-initial-setup/pages/account/um-realm-manager.c:310
msgid "Cannot automatically join this type of domain"
msgstr "نمی‌توان به‌طور خودکار وارد این گونه از دامنه‌ها شد"

#: gnome-initial-setup/pages/account/um-realm-manager.c:373
#, c-format
msgid "No such domain or realm found"
msgstr "چنین دامنه یا ناحیه‌ای پیدا نشد"

#: gnome-initial-setup/pages/account/um-realm-manager.c:782
#: gnome-initial-setup/pages/account/um-realm-manager.c:796
#, c-format
msgid "Cannot log in as %s at the %s domain"
msgstr "نمی‌توان به‌عنوان %s وارد دامنهٔ %s شد"

#: gnome-initial-setup/pages/account/um-realm-manager.c:788
msgid "Invalid password, please try again"
msgstr "گذرواژهٔ نامعتبر، لطفاً دوباره تلاش کنید"

#: gnome-initial-setup/pages/account/um-realm-manager.c:801
#, c-format
msgid "Couldn’t connect to the %s domain: %s"
msgstr "نمی‌توان به دامنهٔ %s متصل شد: %s"

#: gnome-initial-setup/pages/account/um-utils.c:245
msgid "Sorry, that user name isn’t available. Please try another."
msgstr "متأسّفانه این نام کاربری موجود نیست. لطفاًً موردی دیگر را بیازمایید."

#: gnome-initial-setup/pages/account/um-utils.c:248
#, c-format
msgid "The username is too long."
msgstr "نام‌کاربری خیلی طولانی است."

#: gnome-initial-setup/pages/account/um-utils.c:251
msgid "The username cannot start with a “-”."
msgstr "نام‌کاربری نمی‌تواند با «-» شروع شود."

#: gnome-initial-setup/pages/account/um-utils.c:254
msgid ""
"The username should only consist of upper and lower case letters from a-z, "
"digits and the following characters: . - _"
msgstr ""
"نام‌کاربری باید فقط حاوی حروف بزرگ و کوچک از بازهٔ a-z، اعداد و نویسه‌های مقابل "
"باشد: . - _"

#: gnome-initial-setup/pages/account/um-utils.c:258
msgid "This will be used to name your home folder and can’t be changed."
msgstr "این نام برای شاخهٔ خانگیتان استفاده شده و قابل تغییر نخواهد بود."

#: gnome-initial-setup/pages/eulas/gis-eula-page.c:301
#: gnome-initial-setup/pages/eulas/gis-eula-page.ui:21
msgid "License Agreements"
msgstr "قراردادهای پروانه"

#: gnome-initial-setup/pages/eulas/gis-eula-page.ui:50
msgid ""
"I have _agreed to the terms and conditions in this end user license agreement."
msgstr "من با شرایط و ضوابطِ این قرارداد کاربر نهایی _موافقت کرده‌ام."

#: gnome-initial-setup/pages/goa/gis-goa-page.c:87
msgid "Add Account"
msgstr "افزودن حساب"

#: gnome-initial-setup/pages/goa/gis-goa-page.c:339
msgid "Online Accounts"
msgstr "حساب‌های برخط"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:39
msgid "Connect Your Online Accounts"
msgstr "حساب‌های برخطتان را وصل کنید"

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:58
msgid ""
"Connect your accounts to easily access your email, online calendar, contacts, "
"documents and photos."
msgstr ""
"برای دسترسی راحت به رایانه‌نامه، تثویم برخط، آشنایان، سندها و عمس‌هایتان، "
"حساب‌هایتان را وصل کنید."

#: gnome-initial-setup/pages/goa/gis-goa-page.ui:95
msgid ""
"Accounts can be added and removed at any time from the Settings application."
msgstr "حساب‌ها می‌توانند در هر زمانی از طریق برنامهٔ تنظیمات حذف یا اضافه شوند."

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:240
msgid "Preview"
msgstr "پیش‌نمایش"

#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:299
#: gnome-initial-setup/pages/language/cc-language-chooser.c:222
#: gnome-initial-setup/pages/region/cc-region-chooser.c:210
msgid "More…"
msgstr "بیش‌تر…"

#. Translators: a search for input methods or keyboard layouts
#. * did not yield any results
#.
#: gnome-initial-setup/pages/keyboard/cc-input-chooser.c:321
msgid "No inputs found"
msgstr "ورودی‌ای پیدا نشد"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.c:480
#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:30
msgid "Typing"
msgstr "تایپ"

#: gnome-initial-setup/pages/keyboard/gis-keyboard-page.ui:42
msgid "Select your keyboard layout or an input method."
msgstr "چیدمان صفحه‌کلیدتان یا روشی ورودی را برگزینید."

#: gnome-initial-setup/pages/language/cc-language-chooser.c:238
msgid "No languages found"
msgstr "هیچ زبانی پیدا نشد"

#: gnome-initial-setup/pages/language/gis-language-page.c:304
msgid "Welcome"
msgstr "خوش‌آمدید"

#. Translators: This is meant to be a warm, engaging welcome message,
#. * like greeting somebody at the door. If the exclamation mark is not
#. * suitable for this in your language you may replace it.
#.
#: gnome-initial-setup/pages/language/gis-welcome-widget.c:132
msgid "Welcome!"
msgstr "خوش‌آمدید!"

#: gnome-initial-setup/pages/network/gis-network-page.c:313
msgctxt "Wireless access point"
msgid "Other…"
msgstr "دیگر…"

#: gnome-initial-setup/pages/network/gis-network-page.c:398
msgid "Wireless networking is disabled"
msgstr "شبکه‌های بی‌سیم از کار افتاده‌اند."

#: gnome-initial-setup/pages/network/gis-network-page.c:405
msgid "Checking for available wireless networks"
msgstr "درحال بررسی برای شبکه‌های بی‌سیم موجود"

#: gnome-initial-setup/pages/network/gis-network-page.c:713
msgid "Network"
msgstr "شبکه"

#: gnome-initial-setup/pages/network/gis-network-page.ui:33
msgid "Wi-Fi"
msgstr "وای‌فای"

#: gnome-initial-setup/pages/network/gis-network-page.ui:45
msgid ""
"Connecting to the Internet will enable you to set the time, add your details, "
"and enable you to access your email, calendar, and contacts. It is also "
"necessary for enterprise login accounts."
msgstr ""
"اتّصال به اینترنت شما را قادر می‌سازد که زمان را تنظیم کنید، جزئیاتتان را "
"بیفزایید، و به پست‌الکترونیکی، تقویم و آشناهای خود دسترسی پیدا کنید. هم‌چنین این "
"کار برای حساب‌های ورود تجاری ضروری است."

#: gnome-initial-setup/pages/network/gis-network-page.ui:103
msgid "No wireless available"
msgstr "هیچ شبکهٔ بی‌سیمی در دسترس نیست"

#: gnome-initial-setup/pages/network/gis-network-page.ui:118
msgid "Turn On"
msgstr "روشن کردن"

#: gnome-initial-setup/pages/password/gis-password-page.c:142
msgid "This is a weak password."
msgstr "این گذرواژه ضعیف است."

#: gnome-initial-setup/pages/password/gis-password-page.c:148
msgid "The passwords do not match."
msgstr "گذرواژه‌ها مطابقت ندارند."

#: gnome-initial-setup/pages/password/gis-password-page.c:270
msgid "Password"
msgstr "گذرواژه"

#: gnome-initial-setup/pages/password/gis-password-page.ui:34
msgid "Set a Password"
msgstr "تنظیم یک گذرواژه"

#: gnome-initial-setup/pages/password/gis-password-page.ui:47
msgid "Be careful not to lose your password."
msgstr "مراقب باشید گذرواژه‌تان را گم نکنید."

#: gnome-initial-setup/pages/password/gis-password-page.ui:98
msgid "_Confirm"
msgstr "_تأیید"

#: gnome-initial-setup/pages/password/pw-utils.c:81
msgctxt "Password hint"
msgid "The new password needs to be different from the old one."
msgstr "گذرواژهٔ جدید باید با گذرواژهٔ پیشین متفاوت باشد."

#: gnome-initial-setup/pages/password/pw-utils.c:83
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing some letters and "
"numbers."
msgstr ""
"این گذواژه بسیار شبیه پیشین است. سعی کنید تعدادی از حروف و اعداد را تغییر دهید"

#: gnome-initial-setup/pages/password/pw-utils.c:85
#: gnome-initial-setup/pages/password/pw-utils.c:93
msgctxt "Password hint"
msgid ""
"This password is very similar to your last one. Try changing the password a bit "
"more."
msgstr ""
"این گذواژه بسیار شبیه پیشین است. سعی کنید گذرواژه را کمی بیش‌تر تغییر دهید."

#: gnome-initial-setup/pages/password/pw-utils.c:87
msgctxt "Password hint"
msgid ""
"This is a weak password. A password without your user name would be stronger."
msgstr "این گذرواژه ضعیف است. گذرواژه‌ای بدون نام‌کاربریتان قوی‌تر است."

#: gnome-initial-setup/pages/password/pw-utils.c:89
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid using your name in the password."
msgstr "این گذرواژه ضعیف است. سعی کنید از استفاده از نامتان در گذرواژه بپرهیزید."

#: gnome-initial-setup/pages/password/pw-utils.c:91
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid some of the words included in the "
"password."
msgstr ""
"این گذرواژه ضعیف است. سعی کنید از تعدادی از واژگان استفاده شده در گذرواژه "
"بپرهیزید."

#: gnome-initial-setup/pages/password/pw-utils.c:95
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid common words."
msgstr "این گذرواژه ضعیف است. سعی کنید از واژگان معمول بپرهیزید."

#: gnome-initial-setup/pages/password/pw-utils.c:97
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid reordering existing words."
msgstr "این گذرواژه ضعیف است. سعی کنید از چایگشت واژگان موجود بپرهیزید."

#: gnome-initial-setup/pages/password/pw-utils.c:99
msgctxt "Password hint"
msgid "This is a weak password. Try to use more numbers."
msgstr "این گذرواژه ضعیف است. سعی کنید اعداد بیش‌تری به کار ببرید."

#: gnome-initial-setup/pages/password/pw-utils.c:101
msgctxt "Password hint"
msgid "This is a weak password. Try to use more uppercase letters."
msgstr "این گذرواژه ضعیف است. سعی کنید از حروف بزرگ بیش‌تری استفاده کنید."

#: gnome-initial-setup/pages/password/pw-utils.c:103
msgctxt "Password hint"
msgid "This is a weak password. Try to use more lowercase letters."
msgstr "این گذرواژه ضعیف است. سعی کنید از حروف کوچک بیش‌تری استفاده کنید."

#: gnome-initial-setup/pages/password/pw-utils.c:105
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use more special characters, like punctuation."
msgstr ""
"این گذرواژه ضعیف است. سعی کنید از نویسه‌های خاص بیش‌تری، مثل نقطه استفاده کنید."

#: gnome-initial-setup/pages/password/pw-utils.c:107
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to use a mixture of letters, numbers and "
"punctuation."
msgstr ""
"این گذرواژه ضعیف است. سعی کنید تلفیقی از حروف، اعداد و نشانه‌ها را استفاده کنید."

#: gnome-initial-setup/pages/password/pw-utils.c:109
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid repeating the same character."
msgstr "این گذرواژه ضعیف است. سعی کنید از تکرار یک نویسه بپرهیزید."

#: gnome-initial-setup/pages/password/pw-utils.c:111
msgctxt "Password hint"
msgid ""
"This is a weak password. Try to avoid repeating the same type of character: you "
"need to mix up letters, numbers and punctuation."
msgstr ""
"این گذرواژه ضعیف است. سعی کنید از تکرار یک گونه نویسه بپرهیزید: لازم است حروف، "
"اعداد و نشانه‌ها را با هم تلفیق کنید."

#: gnome-initial-setup/pages/password/pw-utils.c:113
msgctxt "Password hint"
msgid "This is a weak password. Try to avoid sequences like 1234 or abcd."
msgstr "این گذرواژه ضعیف است. سعی کنید از دنباله‌هایی چون ۱۲۳۴ و abcd اجتناب کنید."

#: gnome-initial-setup/pages/password/pw-utils.c:115
msgctxt "Password hint"
msgid "This is a weak password. Try to add more letters, numbers and punctuation."
msgstr ""
"این گذرواژه ضعیف است. سعی کنید حروف، اعداد و نشانه‌های بیش‌تری به کار ببرید."

#: gnome-initial-setup/pages/password/pw-utils.c:117
msgctxt "Password hint"
msgid "Mix uppercase and lowercase and try to use a number or two."
msgstr "حروف بزرگ و کوچک، همراه با یکی دو عدد را با هم تلفیق کنید."

#: gnome-initial-setup/pages/password/pw-utils.c:119
msgctxt "Password hint"
msgid ""
"Adding more letters, numbers and punctuation will make the password stronger."
msgstr "افزودن حروف، اعداد و نشانه‌های بیش‌تر، گذرواژه را قوی‌تر می‌کند."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:110
#, c-format
msgid ""
"Sending reports of technical problems helps us to improve %s. Reports are sent "
"anonymously and are scrubbed of personal data."
msgstr ""
"ارسال گزارشات از مشکلات تکنیکی به ما برای گسترش %s کمک می‌کند. گزارش‌ها بصورت "
"ناشناس ارسال می‌شوند و عاری از اطلاعات شخصی هستند."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME" if we can't
#. * detect any distribution.
#.
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:120
#, c-format
msgid "Problem data will be collected by %s:"
msgstr "اطلاعات مربوط به اشکالات توسط %s جمع‌آوری می‌شود:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:121
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:181
#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:256
msgid "Privacy Policy"
msgstr "سیاست محرمانگی"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:181
msgid "Uses Mozilla Location Service:"
msgstr "استفاده از سرویس مکانی موزیلا:"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.c:292
#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:32
msgid "Privacy"
msgstr "محرمانگی"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:49
msgid "Location Services"
msgstr "خدمات مکانی"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:70
msgid ""
"Allows applications to determine your geographical location. An indication is "
"shown when location services are in use."
msgstr ""
"اجازه می‌دهد تا برنامه‌ها از مکان جغرافیایی شما مطلع شوند. یک نشانگر در هنگام "
"استفاده از این سرویس نمایش داده خواهد شد."

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:92
msgid "Automatic Problem Reporting"
msgstr "گزارش خودکار اشکالات"

#: gnome-initial-setup/pages/privacy/gis-privacy-page.ui:128
msgid "Privacy controls can be changed at any time from the Settings application."
msgstr "واپایش‌های محرمانگی می‌توانند در هر زمانی از برنامهٔ تنظیمات تغییر یابند."

#: gnome-initial-setup/pages/region/cc-region-chooser.c:227
msgid "No regions found"
msgstr "هیچ ناحیه‌ای پیدا نشد"

#: gnome-initial-setup/pages/region/gis-region-page.c:223
#: gnome-initial-setup/pages/region/gis-region-page.ui:31
msgid "Region"
msgstr "ناحیه"

#: gnome-initial-setup/pages/region/gis-region-page.ui:44
msgid "Choose your country or region."
msgstr "کشور یا ناحیهٔ خود را برگزینید."

#: gnome-initial-setup/pages/software/gis-software-page.c:184
msgid "Software Repositories"
msgstr "مخازن نرم‌افزاری"

#. TRANSLATORS: this is the third party repositories info bar.
#: gnome-initial-setup/pages/software/gis-software-page.c:188
msgid "Access additional software from selected third party sources."
msgstr "دسترسی به برنامه‌های اضافی از منابع خارجی گزیده."

#. TRANSLATORS: this is the third party repositories info bar.
#: gnome-initial-setup/pages/software/gis-software-page.c:192
msgid ""
"Some of this software is proprietary and therefore has restrictions on use, "
"sharing, and access to source code."
msgstr ""
"برخی از این نرم‌افزارها انحصاری بوده و بنابراین در استفاده، هم‌رسانی و دسترسی به "
"کد مبدأ، محدودند."

#: gnome-initial-setup/pages/software/gis-software-page.ui:31
msgid "Additional Software Repositories"
msgstr "مخازن نرم‌افزاری اضافی"

#: gnome-initial-setup/pages/software/gis-software-page.ui:55
msgid "<a href=\"more\">Find out more…</a>"
msgstr "<a href=\"more\">اطلاعات بیش‌تر…</a>"

#: gnome-initial-setup/pages/software/gis-software-page.ui:69
msgid "Third Party Repositories"
msgstr "مخازن خارجی"

#: gnome-initial-setup/pages/software/gis-software-page.ui:165
msgid ""
"Proprietary software typically has restrictions on how it can be used and on "
"access to source code. This prevents anyone but the software owner from "
"inspecting, improving or learning from its code."
msgstr ""
"نرم‌افزارهای تجاری معمولا در نحوهٔ استفاده و دسترسی به کد مبدأ محدودند. این مورد "
"هر کسی را جز مالک نرم‌افزار برای بازرسی، بهبود یا یادگیری از کدهای آن محدود "
"می‌کند."

#: gnome-initial-setup/pages/software/gis-software-page.ui:177
msgid ""
"In contrast, Free Software can be freely run, copied, distributed, studied and "
"modified."
msgstr ""
"در مقابل، نرم‌افزارهای آزاد می‌توانند آزادانه اجرا، رونوشت، توزیع، مطالعه و تغییر "
"پیدا کنند."

#. Translators: the parameter here is the name of a distribution,
#. * like "Fedora" or "Ubuntu". It falls back to "GNOME 3" if we can't
#. * detect any distribution.
#: gnome-initial-setup/pages/summary/gis-summary-page.c:271
#, c-format
msgid "_Start Using %s"
msgstr "_استفاده از %s را شروع کنید"

#: gnome-initial-setup/pages/summary/gis-summary-page.c:301
msgid "Ready to Go"
msgstr "آماده برای حرکت"

#: gnome-initial-setup/pages/summary/gis-summary-page.ui:64
msgid "You’re ready to go!"
msgstr "شما آماده‌اید!"

#. Translators: "city, country"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:236
#, c-format
msgctxt "timezone loc"
msgid "%s, %s"
msgstr "%s، %s"

#. Translators: UTC here means the Coordinated Universal Time.
#. * %:::z will be replaced by the offset from UTC e.g. UTC+02
#.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:273
msgid "UTC%:::z"
msgstr "UTC%:::z"

#. Translators: This is the time format used in 12-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:277
msgid "%l:%M %p"
msgstr "%Ol:%OM %p"

#. Translators: This is the time format used in 24-hour mode.
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:280
msgid "%R"
msgstr "%OH:%OM"

#. Translators: "timezone (utc shift)"
#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:283
#, c-format
msgctxt "timezone map"
msgid "%s (%s)"
msgstr "%s (%s)"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.c:426
#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:33
msgid "Time Zone"
msgstr "منظقهٔ زمانی"

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:53
msgid ""
"The time zone will be set automatically if your location can be found. You can "
"also search for a city to set it yourself."
msgstr ""
"منطقه زمانی چنانچه امکان پیدا کردن مکان شما باشد، بطور خودکار تنظیم می‌شود. شما "
"همچنین می‌توانید یک شهر را برای تنظیم دستی جست‌وجو کنید."

#: gnome-initial-setup/pages/timezone/gis-timezone-page.ui:82
msgid "Please search for a nearby city"
msgstr "لطفاًً برای شهری نزدیک جست‌وجو کنید"

#~ msgid "Disable image"
#~ msgstr "غیرفعال‌کردن عکس"

#~ msgctxt "Password hint"
#~ msgid "Try to avoid common words."
#~ msgstr "سعی کنید از کلمات معمول استفاده نکنید."

#~ msgctxt "Password hint"
#~ msgid "Try to use more numbers."
#~ msgstr "سعی کنید از اعداد بیشتری استفاده کنید."

#~ msgctxt "Password hint"
#~ msgid ""
#~ "Password needs to be longer. Try to add more letters, numbers and "
#~ "punctuation."
#~ msgstr "سعی کنید از تلفیقی از حروف، اعداد و نشانه‌ها استفاده کنید."

#~ msgid ""
#~ "Proprietary software sources provide access to additional software, "
#~ "including web browsers and games. This software typically has restrictions "
#~ "on use and access to source code, and is not provided by %s."
#~ msgstr ""
#~ "منابع نرم‌افزاری تجاری امکان دسترسی به نرم‌افزارهای اضافی، شامل مرورگرهی وب و "
#~ "بازی‌ها رو به شما می‌دهند. این نرم‌افزارها معمولا برای استفاده و دسترسی کدمنبع "
#~ "شامل محدودیت‌هایی می‌شوند، و توسط %s فراهم نشده‌اند."

#~ msgid "Proprietary Software Sources"
#~ msgstr "منابع نرم‌افزاری تجاری"

#~ msgid "A user with the username '%s' already exists."
#~ msgstr "یک کاربر به نام‌کاربری «%s» از قبل وجود دارد."

#~ msgid "_Verify"
#~ msgstr "_تایید"

#~ msgctxt "Password strength"
#~ msgid "Strength: Weak"
#~ msgstr "قدرت: ضعیف"

#~ msgctxt "Password strength"
#~ msgid "Strength: Low"
#~ msgstr "قدرت: کم"

#~ msgctxt "Password strength"
#~ msgid "Strength: Medium"
#~ msgstr "قدرت: معمولی"

#~ msgctxt "Password strength"
#~ msgid "Strength: Good"
#~ msgstr "قدرت: خوب"

#~ msgctxt "Password strength"
#~ msgid "Strength: High"
#~ msgstr "قدرت: بالا"

#~ msgid "Are these the right details? You can change them if you want."
#~ msgstr "آیا این‌ها جزئیات صحیحی هستند؟ اگر بخواهید می‌توانید آن‌ها را تغییر دهید."

#~ msgid "You can review your online accounts (and add others) after setup."
#~ msgstr ""
#~ "شما می‌توانید پس از راه‌اندازی، حساب‌های برخط خود را مرور کرده و حساب‌های جدیدی "
#~ "بیفزایید."

#~ msgid ""
#~ "Thank you for choosing %s.\n"
#~ "We hope that you love it."
#~ msgstr ""
#~ "برای انتخاب %s از شما ممنون هستیم\n"
#~ "امیدواریم دوستش داشته باشید."

#~ msgid ""
#~ "We think that your time zone is %s. Press Next to continue or search for a "
#~ "city to manually set the time zone."
#~ msgstr ""
#~ "ما فکر می‌کنیم منطقه زمانی شما %s باشد. برای ادامه «بعدی» را انتخاب کنید یا "
#~ "یک شهر جست‌وجو کنید تا منطقه زمانی را به‌طور خودکار تغییر دهید."

#~ msgid "Cancel"
#~ msgstr "لغو"

#~ msgid "Your username cannot be changed after setup."
#~ msgstr "نام کاربری شما نمی‌تواند پس از راه‌اندازی عوض شود."

#~ msgid "No password"
#~ msgstr "بدون گذرواژه"

#~ msgctxt "Password strength"
#~ msgid "Too short"
#~ msgstr "خیلی کوتاه"

#~ msgctxt "Password strength"
#~ msgid "Not good enough"
#~ msgstr "به اندازه کافی مناسب نیست"

#~ msgctxt "Password strength"
#~ msgid "Weak"
#~ msgstr "ضعیف"

#~ msgctxt "Password strength"
#~ msgid "Fair"
#~ msgstr "نسبتاً خوب"

#~ msgctxt "Password strength"
#~ msgid "Good"
#~ msgstr "خوب"

#~ msgctxt "Password strength"
#~ msgid "Strong"
#~ msgstr "قوی"

#~ msgid "Login"
#~ msgstr "ورود به سیستم"

#~ msgid "Create a Local Account"
#~ msgstr "ساخت یک حساب محلی"

#~ msgid "page 1"
#~ msgstr "صفحه ۱"

#~ msgid "Create an Enterprise Account"
#~ msgstr "ساخت یک حساب تجاری"

#~| msgctxt "Wireless access point"
#~| msgid "Other…"
#~ msgctxt "Online Account"
#~ msgid "Other"
#~ msgstr "موارد دیگر"

#~ msgid "Mail"
#~ msgstr "پست‌الکترونیکی"

#~ msgid "Contacts"
#~ msgstr "آشناها"

#~ msgid "Chat"
#~ msgstr "گپ"

#~ msgid "Resources"
#~ msgstr "منابع"

#~ msgid "Error creating account"
#~ msgstr "خطا در ساخت حساب"

#~ msgid "Error removing account"
#~ msgstr "خطا در حذف حساب"

#~ msgid "Are you sure you want to remove the account?"
#~ msgstr "آیا مطمئنید که می‌خواهید این حساب را حذف کنید؟"

#~ msgid "This will not remove the account on the server."
#~ msgstr "این کار حساب شما را بر روی کارگزار حذف نمی‌کند."

#~ msgid "_Remove"
#~ msgstr "_حذف"

#~ msgid "Connect to your existing data in the cloud"
#~ msgstr "به اطلاعات موجود خود در ابرها متصل شوید"

#~| msgid "Add Account"
#~ msgid "_Add Account"
#~ msgstr "اضافه کردن _حساب"

#~| msgctxt "Wireless access point"
#~| msgid "Other…"
#~ msgid "Other"
#~ msgstr "موارد دیگر"

#~| msgid "Add Input Source"
#~ msgid "Add an Input Source"
#~ msgstr "اضافه کردن یک منبع ورودی"

#~ msgid "Search for a location"
#~ msgstr "جستجو برای یک مکان"

#~ msgid "_Determine your location automatically"
#~ msgstr "مکان خود را خودکار _تشخیص دهید"

#~ msgid "No network devices found."
#~ msgstr "هیچ دستگاه شبکه‌ای یافت نشد."

#~ msgid "Thank You"
#~ msgstr "متشکریم"

#~ msgid "Your computer is ready to use."
#~ msgstr "رایانه شما آماده استفاده است."

#~ msgid "You may change these options at any time in Settings."
#~ msgstr "شما می‌توانید این گزینه‌ها را در هر زمانی در تنظیمات سیستم تغییر دهید."

#~ msgid "_Start using GNOME 3"
#~ msgstr "_شروع به استفاده از گنوم ۳"

#~ msgid "_Back"
#~ msgstr "_قبلی"

#~ msgid "Keyboard Layout"
#~ msgstr "چیدمان صفحه‌کلید"

#~ msgid "Select input sources"
#~ msgstr "انتخاب منابع ورودی"

#~ msgid "Remove Input Source"
#~ msgstr "حذف منبع ورودی"

#~ msgid "Move Input Source Up"
#~ msgstr "بالابردن منبع ورودی"

#~ msgid "Input Source Settings"
#~ msgstr "تنظیمات منبع ورودی"

#~ msgid "Select an input source"
#~ msgstr "انتخاب یک منبع ورودی"

#~ msgid "Create Local Account"
#~ msgstr "ایجاد حساب محلی"

#~ msgid "_Done"
#~ msgstr "_پایان"

#~ msgid "_Require a password to use this account"
#~ msgstr "_برای استفاده از این حساب به یک گذرواژه نیاز است"

#~ msgid "_Act as administrator of this computer"
#~ msgstr "_به‌عنوان مدیر این رایانه"

#~ msgid "Choose How to Login"
#~ msgstr "چگونگی ورود به سیستم را انتخاب کنید"

#~ msgid "Remove"
#~ msgstr "حذف"

#~ msgid "English"
#~ msgstr "انگلیسی"

#~ msgid "British English"
#~ msgstr "انگلیسی بریتانیایی"

#~ msgid "German"
#~ msgstr "آلمانی"

#~ msgid "French"
#~ msgstr "فرانسه"

#~ msgid "Spanish"
#~ msgstr "اسپانیایی"

#~ msgid "Chinese (simplified)"
#~ msgstr "چینی (ساده شده)"

#~ msgid "Unspecified"
#~ msgstr "مشخص نشده"

#~ msgid "Use %s"
#~ msgstr "استفاده از %s"

#~ msgid "Show _all"
#~ msgstr "نمایش _همه"

#~ msgid "Enjoy GNOME!"
#~ msgstr "از گنوم لذت ببرید!"

#~ msgid "New to GNOME 3 and need help finding your way around?"
#~ msgstr "در گنوم ۳ تازه‌کارید و به کمی کمک برای پیدا کردن راه نیاز دارید؟"

#~ msgid "_Take a Tour"
#~ msgstr "_راهنمایی بگیرید"
